//
//  GameScene.swift
//  Game
//
//  Created by ivan piskun on 20.08.2018.
//  Copyright © 2018 ivan piskun. All rights reserved.
//

import SpriteKit
import GameplayKit


class GameScene: SKScene,SKPhysicsContactDelegate {
    
    var leftCar = SKSpriteNode()
    
    var canMove = false
    var leftToMoveLeft = true
    
    var leftCarAtRight = false
    var centerPoint : CGFloat!
    var score = 0
    
    let leftCarMinimumX: CGFloat = -200
    let leftCarMaximumx: CGFloat = 200

    
    var countDown = 1
    var stopEverything = true
    var scoreText = SKLabelNode()
    
    var gameSettings = Settings.sharedInstance
    
    override func didMove(to view: SKView) {
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        setUp()
        physicsWorld.contactDelegate = self
        Timer.scheduledTimer(timeInterval: TimeInterval(0.1), target: self, selector: #selector (GameScene.createRoadStrip), userInfo: nil, repeats: true)
        
        Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector (GameScene.startCountDown), userInfo: nil, repeats: true)
        
        //скорость появления машин
        Timer.scheduledTimer(timeInterval: TimeInterval(CarTrafic().randomBetweenTwoNumbers(firstNumber: 1.5, secondNumber: 2.5)), target: self, selector: #selector (GameScene.leftTraffic), userInfo: nil, repeats: true)
        
        Timer.scheduledTimer(timeInterval: TimeInterval(0.5), target: self, selector: #selector (GameScene.removeItems), userInfo: nil, repeats: true)
        
        let deadTime = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: deadTime) {
            Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector (GameScene.increaseScore), userInfo: nil, repeats: true)
        }
    }
    
    override func update(_ currentTime: TimeInterval){
        if canMove{
            move(leftSide:leftToMoveLeft)
        }
     showRoadStrip()
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody = SKPhysicsBody()
        var secondBody = SKPhysicsBody()
//&&&&&&&&&&&&&&&&
        if contact.bodyA.node?.name == "leftCar" {
        firstBody = contact.bodyA
            secondBody = contact.bodyB
        }else{
            firstBody = contact.bodyB
            secondBody = contact.bodyA
    }
        firstBody.node?.removeFromParent()
        afterCollision()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let touchLocation = touch.location(in: self)
            if touchLocation.x > centerPoint {
                if leftCarAtRight{
                    leftCarAtRight = false
                    leftToMoveLeft = true
                }
            }else {
                leftCarAtRight = true
                leftToMoveLeft = false
                
            }
            canMove = true
        }
    }
    
    func setUp() {
        leftCar = self.childNode(withName: "leftCar") as! SKSpriteNode
        centerPoint = self.frame.size.width / self.frame.size.height
        
        leftCar.physicsBody?.categoryBitMask = ColliderType.CAR_COLLIDER
        leftCar.physicsBody?.contactTestBitMask = ColliderType.ITEM_COLLIDER
        leftCar.physicsBody?.collisionBitMask = 0
        
//очки
        let scoreBackGround = SKShapeNode(rect: CGRect(x:-self.size.width/2 + 70 , y: self.size.height/2 - 130 , width: 180 , height: 80), cornerRadius: 20)
        scoreBackGround.zPosition = 4
        scoreBackGround.fillColor = SKColor.black.withAlphaComponent(0.3)
        scoreBackGround.strokeColor = SKColor.black.withAlphaComponent(0.3)
        addChild(scoreBackGround)
        
        scoreText.name = "score"
        scoreText.fontName = "AvenirNext-Bold"
        scoreText.text = "0"
        scoreText.fontColor = SKColor.white
        scoreText.position = CGPoint(x: -self.size.width/2 + 160, y: self.size.height/2 - 110)
        scoreText.fontSize = 50
        scoreText.zPosition = 4
        addChild(scoreText)
    }
    
    @objc func createRoadStrip(){
        let leftRoadStrip = SKShapeNode(rectOf: CGSize(width: 10, height: 40))
        leftRoadStrip.strokeColor = SKColor.white
        leftRoadStrip.fillColor = SKColor.white
        leftRoadStrip.alpha = 0.4
        leftRoadStrip.name = "leftRoadStrip"
        leftRoadStrip.zPosition = 10
        leftRoadStrip.position.x = 0
        leftRoadStrip.position.y = 700
        addChild(leftRoadStrip)
    }
//скорость
    func showRoadStrip(){
        
        enumerateChildNodes(withName: "leftRoadStrip", using: {(roadStrip, stop ) in
            let strip = roadStrip as! SKShapeNode
            strip.position.y -= 30
        })
        enumerateChildNodes(withName: "whiteCar", using: {(leftCar, stop ) in
            let car = leftCar as! SKSpriteNode
            car.position.y -= 20
        })
        enumerateChildNodes(withName: "blueCar", using: {(rightCar, stop ) in
            let car = rightCar as! SKSpriteNode
            car.position.y -= 20
        })
        enumerateChildNodes(withName: "redCar", using: {(rightCar, stop ) in
            let car = rightCar as! SKSpriteNode
            car.position.y -= 20
        })
        enumerateChildNodes(withName: "twocollorCar", using: {(leftCar, stop ) in
            let car = leftCar as! SKSpriteNode
            car.position.y -= 20
        })
        enumerateChildNodes(withName: "blueCar2", using: {(leftCar, stop ) in
            let car = leftCar as! SKSpriteNode
            car.position.y -= 20
        })
    }


    @objc func removeItems() {
    for child in children {
        if child.position.y < -self.size.height - 100{
            child.removeFromParent()
        }
    }
}

    //машина поворачивает  с ускорением
func move(leftSide:Bool){
    if leftSide{
        leftCar.position.x -= 30
        if leftCar.position.x < leftCarMinimumX{
            leftCar.position.x = leftCarMinimumX
        }
    }else{
        leftCar.position.x += 30
        if leftCar.position.x > leftCarMaximumx{
            leftCar.position.x = leftCarMaximumx
        }
    }
}

    @objc func leftTraffic() {
        if !stopEverything{
        let leftTrafficItem : SKSpriteNode!
        let randomNumber = CarTrafic().randomBetweenTwoNumbers(firstNumber: 1, secondNumber: 10)
        switch  Int(randomNumber) {
        case 1...2:
            leftTrafficItem = SKSpriteNode(imageNamed: "whiteCar")
            leftTrafficItem.name = "whiteCar"
            break
        case 3...4:
            leftTrafficItem = SKSpriteNode(imageNamed: "redCar")
            leftTrafficItem.name = "redCar"
            break
        case 5...6:
            leftTrafficItem = SKSpriteNode(imageNamed: "twocollorCar")
            leftTrafficItem.name = "twocollorCar"
            break
        case 7...8:
            leftTrafficItem = SKSpriteNode(imageNamed: "blueCar")
            leftTrafficItem.name = "blueCar"
            break
        case 9...10:
            leftTrafficItem = SKSpriteNode(imageNamed: "blueCar2")
            leftTrafficItem.name = "blueCar2"
            break
        default:
            leftTrafficItem = SKSpriteNode(imageNamed: "whiteCar")
            leftTrafficItem.name = "whiteCar"
        }
        leftTrafficItem.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        leftTrafficItem.zPosition = 10
        let randomNum = CarTrafic().randomBetweenTwoNumbers(firstNumber: 1, secondNumber: 10)
        switch Int(randomNum) {
        case 1...4:
            leftTrafficItem.position.x = -200
            break
        case 5...10:
            leftTrafficItem.position.x = 200
            break
        default:
            leftTrafficItem.position.x = -200
        }
        leftTrafficItem.position.y = 700
            //тут у нас размер машины при контакте засчитывает столкновение
        leftTrafficItem.physicsBody = SKPhysicsBody(circleOfRadius: leftTrafficItem.size.height/3)
        leftTrafficItem.physicsBody?.categoryBitMask = ColliderType.ITEM_COLLIDER
        leftTrafficItem.physicsBody?.collisionBitMask = 0
        leftTrafficItem.physicsBody?.affectedByGravity = false
        
        addChild(leftTrafficItem)
        }
       
    }
    //после удара машин
    func afterCollision(){
        if gameSettings.hightScore < score{
            gameSettings.hightScore = score
        }
        let menuScene = SKScene(fileNamed: "GameMenu")!
        menuScene.scaleMode = .aspectFill
        view?.presentScene(menuScene, transition: SKTransition.doorsCloseHorizontal(withDuration: TimeInterval(2)))
    }
    //таймер начала игры
    @objc func startCountDown() {
        if countDown>0{
            if countDown < 4{
                let countDownLabel = SKLabelNode()
                countDownLabel.fontName = "AvenirNext-Bold"
                countDownLabel.fontColor = SKColor.white
                countDownLabel.fontSize = 300
                countDownLabel.text = String(countDown)
                countDownLabel.position = CGPoint(x: 0, y: 0)
                countDownLabel.zPosition = 300
                countDownLabel.name = "cLabel"
                countDownLabel.horizontalAlignmentMode = .center
                addChild(countDownLabel)
                
                let deadTime = DispatchTime.now() + 0.5
                DispatchQueue.main.asyncAfter(deadline: deadTime, execute: {
                    countDownLabel.removeFromParent()
                    })
            }
            countDown += 1
            if countDown == 4{
                self.stopEverything = false
            }
        }
    }
    
    @objc func increaseScore() {
        if !stopEverything{
            score += 1
            scoreText.text = String(score)
        }
    }
}






