//
//  CarTrafic.swift
//  Game
//
//  Created by ivan piskun on 24.08.2018.
//  Copyright © 2018 ivan piskun. All rights reserved.
//

import Foundation
import UIKit

struct ColliderType {
    static let CAR_COLLIDER : UInt32 = 0
    static let ITEM_COLLIDER : UInt32 = 1
    
}


class CarTrafic : NSObject {
    
    func randomBetweenTwoNumbers(firstNumber : CGFloat , secondNumber : CGFloat) -> CGFloat {
        return CGFloat (arc4random())/CGFloat(UINT32_MAX) * abs(firstNumber - secondNumber) + min(firstNumber, secondNumber)
    }
}

class Settings {
    static let sharedInstance = Settings()
    
    private init() {
        
    }
    
    var hightScore = 0
}
